import React from "react";

class App extends React.Component {
  state = {
    text: ""
  };

  callApi() {
    const API_URL = "http://localhost:8080/testApi";

    fetch(API_URL)
      .then(res => res.text())
      .then(data => this.setState({ text: data }));
  }

  componentDidMount() {
    this.callApi();
  }
  render() {
    return (
      <div>
        <h2>{this.state.text}</h2>
      </div>
    );
  }
}

export default App;
